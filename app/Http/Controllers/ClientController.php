<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Client;

class ClientController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->input('search');
        $clients = Client::where(DB::raw("CONCAT(business_name,' ',street,' ',state)"),'like',"%$search%")->paginate(15);
        return view('Clients.index',compact('search','clients'));
    }

    public function create()
    {
        return view('Clients.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'business_name' => 'required|unique:clients,business_name',
            'state'         => 'required',
            'municipality'  => 'required',
            'street'        => 'required|regex:/^[a-zA-Z]([a-zA-Z-]+\s)+\d{1,4}$/',
            'neighborhood'  => 'required',
            'zipcode'       => 'nullable|numeric|digits:5',
            'phone'         => 'nullable|numeric|digits:10',
            'rfc'           => 'nullable|unique:clients,rfc|min:12|max:13'
        ],[
            'business_name.required' => 'El nombre o razón social es requerido',
            'business_name.unique'   => 'El nombre o razón social ya está en uso',
            'state.required'         => 'El estado es requerido',
            'municipality.required'  => 'El municipio es requerido',
            'street.required'        => 'La calle y número son requeridos',
            'street.regex'           => 'El formato de calle y número es incorrecto',
            'neighborhood.required'  => 'La colonia es requerida',
            'zipcode.numeric'        => 'El código postal debe ser númerico',
            'zipcode.digits'         => 'El código postal debe contener 5 caracteres',
            'phone.numeric'          => 'El teléfono debe ser númerico',
            'phone.digits'           => 'El teléfono debe contener 10 caracteres',
            'rfc.unique'             => 'El RFC ingresado ya está en uso',
            'rfc.min'                => 'El RFC debe contener 12 caracteres como mínimo',
            'rfc.max'                => 'El RFC debe contener 13 caracteres como máximo'
        ]);

        $client = new Client();
        $client->business_name = $request->business_name;
        $client->responsable   = $request->responsable;
        $client->state         = $request->state;
        $client->municipality  = $request->municipality;
        $client->street        = $request->street;
        $client->neighborhood  = $request->neighborhood;
        $client->zipcode       = $request->zipcode;
        $client->phone         = $request->phone;
        $client->rfc           = $request->rfc;

        $client->save();

        return redirect()->route('clients.index')->withStatus('Cliente registrado correctamente');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $client = Client::findOrfail($id);

        return view('Clients.edit', compact('client'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'business_name' => ['required',Rule::unique('clients')->ignore($id)],
            'state'         => 'required',
            'municipality'  => 'required',
            'street'        => 'required|regex:/^[a-zA-Z]([a-zA-Z-]+\s)+\d{1,4}$/',
            'neighborhood'  => 'required',
            'zipcode'       => 'nullable|numeric|digits:5',
            'phone'         => 'nullable|numeric|digits:10',
            'rfc'           => ['nullable',Rule::unique('clients')->ignore($id)]
        ],[
            'business_name.required' => 'El nombre o razón social es requerido',
            'business_name.unique'   => 'El nombre o razón social ya está en uso',
            'state.required'         => 'El estado es requerido',
            'municipality.required'  => 'El municipio es requerido',
            'street.required'        => 'La calle y número son requeridos',
            'street.regex'           => 'El formato de calle y número es incorrecto',
            'neighborhood.required'  => 'La colonia es requerida',
            'zipcode.numeric'        => 'El código postal debe ser númerico',
            'zipcode.digits'         => 'El código postal debe contener 5 caracteres',
            'phone.numeric'          => 'El teléfono debe ser númerico',
            'phone.digits'           => 'El teléfono debe contener 10 caracteres',
            'rfc.unique'             => 'El RFC ingresado ya está en uso',
            'rfc.min'                => 'El RFC debe contener 12 caracteres como mínimo',
            'rfc.max'                => 'El RFC debe contener 13 caracteres como máximo'
        ]);

        $client = Client::findOrfail($id);
        $client->business_name = $request->business_name;
        $client->responsable   = $request->responsable;
        $client->state         = $request->state;
        $client->municipality  = $request->municipality;
        $client->street        = $request->street;
        $client->neighborhood  = $request->neighborhood;
        $client->zipcode       = $request->zipcode;
        $client->phone         = $request->phone;
        $client->rfc           = $request->rfc;

        $client->update();

        return redirect()->route('clients.index')->withStatus('Cliente actualizado correctamente');
    }


    public function destroy($id)
    {
        //
    }
}

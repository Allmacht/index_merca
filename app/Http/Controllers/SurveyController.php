<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Survey;

class SurveyController extends Controller
{
    public function index(Request $request){

        $search = $request->input('search');
        $surveys = Survey::where(DB::raw("CONCAT(name,' ',total_questions)"),'like',"%$search%")->paginate(15);

        return view('Surveys.index',compact('search','surveys'));

    }

    public function create()
    {
        return view('Surveys.create');
    }

    public function store(Request $request)
    {
        // return response()->json($request);
        $validator = Validator::make($request->all(),[
            'name'            => 'required',
            'total_questions' => 'required|numeric',
            'questions'       => 'required|array'
        ],[
            'name.required'            => 'El nombre de la encuesta es requerido',
            'total_questions.required' => 'El total de preguntas es requerido',
            'total_questions.numeric'  => 'El total de preguntas debe ser numérico',
            'questions.required'       => 'La encuesta debe contener preguntas',
            'questions.array'          => 'Las preguntas no son válidas'
        ]);

        if($validator->fails()):
            return response()->json(['errors' => $validator->errors()]);
        endif;

        $survey = new Survey();
        $survey->name            = $request->name;
        $survey->total_questions = $request->total_questions;
        $survey->questions       = json_encode($request->questions);

        $survey->save();

        return response()->json(['survey' => $survey],200);

    }

    public function show($id)
    {
        $survey = Survey::findOrfail($id);
        return view('Surveys.show', compact('survey'));
    }
}

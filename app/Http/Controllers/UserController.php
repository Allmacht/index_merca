<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\User;
use App\Client;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $users = User::where(DB::raw("CONCAT(name,' ',email)"),'like',"%$search%")->paginate(10);
        return view('Users.index', compact('users','search'));
    }

    public function authenticated(){
        $user = array(
            'name' => Auth::user()->name,
            'avatar' => Auth::user()->avatar
        );
        return response()->json(['user' => $user],200);
    }

    public function create(){
        $clients = Client::select('id','business_name')->get();
        return view('Users.create', compact('clients'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'role'                  => 'required|exists:roles,name',
            'client_id'             => 'required_if:role,Cliente|required_if:criterion,Client|exists:clients,id',
            'avatar'                => 'nullable|file|mimes:jpeg,bmp,png,jpg',
            'state'                 => 'required_if:criterion,State',
            'password'              => 'required|min:8',
            'password_confirmation' => 'required|same:password',
            'criterion'             => 'nullable',
        ],[
            'name.required'                  => 'El nombre es requerido',
            'email.required'                 => 'El correo electrónico es requerido',
            'email.email'                    => 'El correo electrónico ingresado no es válido',
            'email.unique'                   => 'El correo electrónico ingresado ya está en uso',
            'role.required'                  => 'Debe seleccionar un rol de la lista',
            'role.exists'                    => 'El rol seleccionado no existe',
            'client_id.exists'               => 'El cliente seleccionado no existe',
            'avatar.file'                    => 'El archivo seleccionado no es válido',
            'avatar.mimes'                   => 'El archivo selccionado no es compatible',
            'client_id.required_if'          => 'Debe seleccionar un cliente de la lista',
            'state.required_if'              => 'Debe seleccionar un estado de la lista',
            'password.required'              => 'La contraseña es requerida',
            'password.min'                   => 'La contraseña debe tener almenos 8 caracteres',
            'password_confirmation.required' => 'La validación de contraseña es requerida',
            'password_confirmation.same'     => 'La validación de contraseña no coincide'

        ]);

        $user = new User();
        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->criterion = $request->criterion;
        $user->password  = bcrypt($request->password);

        if($request->role == 'Cliente'):
            $user->client_id = $request->client_id;
        endif;
        if($request->role == 'Supervisor'):
            if($request->criterion == 'Client'):
                $user->client_id = $request->client_id;
            else:
                $user->state = $request->state;
            endif;
        endif;

        if($request->file('avatar')):
            $file = $request->file('avatar');
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file_path = public_path('/images/avatars');
            $file->move($file_path,$filename);

            $user->avatar = $filename;
        endif;

        $user->save();

        $user->assignRole($request->role);

        return redirect()->route('users.index')->withStatus('Usuario registrado correctamente');
    }

    public function edit($id)
    {
        $user = User::findOrfail($id);
        $clients = Client::select('id','business_name')->get();
        $role = $user->getRoleNames();
        return view('Users.edit',compact('user','role','clients'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'                  => 'required',
            'email'                 => ['required','email',Rule::unique('users')->ignore($id)],
            'role'                  => 'required|exists:roles,name',
            'client_id'             => 'required_if:role,Cliente|required_if:criterion,Client|exists:clients,id',
            'avatar'                => 'nullable|file|mimes:jpeg,bmp,png,jpg',
            'state'                 => 'required_if:criterion,State',
            'password'              => 'nullable|min:8',
            'password_confirmation' => 'nullable|same:password',
            'criterion'             => 'nullable',
        ],[
            'name.required'                  => 'El nombre es requerido',
            'email.required'                 => 'El correo electrónico es requerido',
            'email.email'                    => 'El correo electrónico ingresado no es válido',
            'email.unique'                   => 'El correo electrónico ingresado ya está en uso',
            'role.required'                  => 'Debe seleccionar un rol de la lista',
            'role.exists'                    => 'El rol seleccionado no existe',
            'client_id.exists'               => 'El cliente seleccionado no existe',
            'avatar.file'                    => 'El archivo seleccionado no es válido',
            'avatar.mimes'                   => 'El archivo selccionado no es compatible',
            'client_id.required_if'          => 'Debe seleccionar un cliente de la lista',
            'state.required_if'              => 'Debe seleccionar un estado de la lista',
            'password.min'                   => 'La contraseña debe tener almenos 8 caracteres',
            'password_confirmation.same'     => 'La validación de contraseña no coincide'
        ]);

        $user = User::findOrfail($id);
        $user->name      = $request->name;
        $user->email     = $request->email;
        if($request->criterion):
            $user->criterion = $request->criterion;
        else:
            $user->criterion = null;
        endif;
        if($request->password):
            $user->password = bcrypt($request->password);
        endif;

        if($request->role == 'Cliente'):
            $user->client_id = $request->client_id;
            $user->state ="";
        endif;
        if($request->role == 'Supervisor'):
            if($request->criterion == 'Client'):
                $user->client_id = $request->client_id;
                $user->state = "";
            else:
                $user->client_id = null;
                $user->state = $request->state;
            endif;
        endif;

        if($request->file('avatar')):
            if($user->avatar != null):
                \File::delete('images/avatars/'.$user->avatar);
            endif;
            $file = $request->file('avatar');
            $filename = uniqid().".".$file->getClientOriginalExtension();
            $file_path = public_path('/images/avatars');
            $file->move($file_path,$filename);

            $user->avatar = $filename;
        endif;

        $current_roles = $user->getRoleNames();

        foreach ($current_roles as $current_role) {
            $user->removeRole($current_role);
        }

        $user->update();
        $user->assignRole($request->role);

        return redirect()->route('users.index')->withStatus('Usuario actualizado correctamente');


    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Verifier;

class VerifierController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->input('search');
        $verifiers = Verifier::where(DB::raw("CONCAT(name,' ',last_name,' ',state,' ',municipality,' ',phone)"),'like',"%$search%")->paginate(15);
        return view('Verifiers.index',compact('search','verifiers'));
    }

    public function create()
    {
        return view('Verifiers.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'last_name'    => 'required',
            'gender'       => 'required',
            'age'          => 'required|numeric',
            'civil_status' => 'required',
            'vehicle'      => 'required|boolean',
            'state'        => 'required',
            'municipality' => 'required',
            'phone'        => 'required|numeric|digits_between:10,12',
            'bank'         => 'required',
            'bank_account' => 'required|digits:24',
            'status'       => 'required'
        ],[
            'name.required'         => 'El nombre es requerido',
            'last_name.required'    => 'Los apellidos son requeridos',
            'gender.required'       => 'Seleccione el sexo del verificador',
            'age.required'          => 'La edad es requerida',
            'age.numeric'           => 'La edad ingresada no es válida',
            'civil_status.required' => 'El estado civil es requerido',
            'vehicle.required'      => 'Seleccione una opción de vehículo',
            'state.required'        => 'El estado es requerido',
            'municipality.required' => 'El Municipio es requerido',
            'phone.required'        => 'El teléfono es requerido',
            'phone.numeric'         => 'El teléfono ingresado no es válido',
            'phone.digits_between'  => 'El teléfono debe tener almenos 10 caracteres',
            'bank.required'         => 'Seleccione una opción de banco',
            'bank_account.required' => 'La cuenta bancaría es requerida',
            'bank_account.digits'   => 'La cuenta bancaría debe tener 24 caracteres',
            'status.required'       => 'Seleccione un estatus del verificador'
        ]);

        $verifier = new Verifier();
        $verifier->name         = $request->name;
        $verifier->last_name    = $request->last_name;
        $verifier->age          = $request->age;
        $verifier->gender       = $request->gender;
        $verifier->civil_status = $request->civil_status;
        $verifier->vehicle      = $request->vehicle;
        $verifier->state        = $request->state;
        $verifier->municipality = $request->municipality;
        $verifier->phone        = $request->phone;
        $verifier->bank         = $request->bank;
        $verifier->bank_account = $request->bank_account;
        $verifier->status       = $request->status;

        $verifier->save();

        return redirect()->route('verifiers.index')->withStatus('Verificador registrado correctamente');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $verifier = Verifier::findOrfail($id);
        return view('Verifiers.edit',compact('verifier'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'         => 'required',
            'last_name'    => 'required',
            'gender'       => 'required',
            'age'          => 'required|numeric',
            'civil_status' => 'required',
            'vehicle'      => 'required|boolean',
            'state'        => 'required',
            'municipality' => 'required',
            'phone'        => 'required|numeric|digits_between:10,12',
            'bank'         => 'required',
            'bank_account' => 'required|digits:24',
            'status'       => 'required'
        ],[
            'name.required'         => 'El nombre es requerido',
            'last_name.required'    => 'Los apellidos son requeridos',
            'gender.required'       => 'Seleccione el sexo del verificador',
            'age.required'          => 'La edad es requerida',
            'age.numeric'           => 'La edad ingresada no es válida',
            'civil_status.required' => 'El estado civil es requerido',
            'vehicle.required'      => 'Seleccione una opción de vehículo',
            'state.required'        => 'El estado es requerido',
            'municipality.required' => 'El Municipio es requerido',
            'phone.required'        => 'El teléfono es requerido',
            'phone.numeric'         => 'El teléfono ingresado no es válido',
            'phone.digits_between'  => 'El teléfono debe tener almenos 10 caracteres',
            'bank.required'         => 'Seleccione una opción de banco',
            'bank_account.required' => 'La cuenta bancaría es requerida',
            'bank_account.digits'   => 'La cuenta bancaría debe tener 24 caracteres',
            'status.required'       => 'Seleccione un estatus del verificador'
        ]);

        $verifier = Verifier::findOrfail($id);
        $verifier->name         = $request->name;
        $verifier->last_name    = $request->last_name;
        $verifier->age          = $request->age;
        $verifier->gender       = $request->gender;
        $verifier->civil_status = $request->civil_status;
        $verifier->vehicle      = $request->vehicle;
        $verifier->state        = $request->state;
        $verifier->municipality = $request->municipality;
        $verifier->phone        = $request->phone;
        $verifier->bank         = $request->bank;
        $verifier->bank_account = $request->bank_account;
        $verifier->status       = $request->status;
        $verifier->update();

        return redirect()->route('verifiers.index')->withStatus('Verificador actualizado correctamente');
    }

    public function destroy($id)
    {
        //
    }
}

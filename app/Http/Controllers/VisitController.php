<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Visit;
use App\Client;
use App\Survey;

class VisitController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        $visits = Visit::select('*','visits.id as id','visits.state as state','visits.street as street','visits.municipality as municipality')
                    ->join('clients','visits.client_id','clients.id')
                    ->where(DB::raw("CONCAT(clients.business_name,' ',visits.street,' ',visits.state,' ',visits.municipality,' ',visits.status)"),'like',"%$search%")
                    ->paginate(10);
        $clients = Client::select('id','business_name')->get();
        $surveys = Survey::select('id','name')->get();

        return view('Visits.index',compact('search','visits','clients','surveys'));
    }

    public function create(){
        $clients = Client::select('id','business_name')->get();
        if($clients->isEmpty())
            return redirect()->route('visits.index')->withErrors('No existen clientes registrados');
        $surveys = Survey::select('id','name')->get();

        return view('Visits.create',compact('clients','surveys'));
    }

    public function store(Request $request){
        $request->validate([
            'client_id'    => 'required|numeric|exists:clients,id',
            'name'         => 'required',
            'state'        => 'required',
            'municipality' => 'required',
            'street'       => 'required|regex:/^[a-zA-Z]([a-zA-Z-]+\s)+\d{1,4}$/',
            'neighborhood' => 'required',
            'zipcode'      => 'nullable|numeric|digits:5',
            'phone'        => 'nullable|numeric|min:10',
            'survey_id'    => 'nullable|numeric|exists:surveys,id'
        ],[
            'client_id.required'    => 'El cliente es requerido',
            'client_id.numeric'     => 'El cliente seleccionado no es válido',
            'client_id.exists'      => 'El cliente seleccionado no existe',
            'name.required'         => 'El nombre de la persona o empresa es requerido',
            'state.required'        => 'El estado es requerido',
            'municipality.required' => 'El municipio es requerido',
            'street.required'       => 'La calle es requerida',
            'street.regex'          => 'La calle ingresada no es válida',
            'neighborhood.required' => 'La colonia es requerida',
            'zipcode.numeric'       => 'El código postal no es válido',
            'zipcode.digits'        => 'El código postal debe tener 5 caracteres',
            'phone.numeric'         => 'El teléfono ingresado no es válido',
            'phone.min'             => 'El teléfono debe tener mínimo 10 caracteres',
            'survey_id.numeric'     => 'La encuesta seleccionada no es válida',
            'survey_id.exists'      => 'La encuesta seleccionada no existe'
        ]);

        $visit = new Visit();

        $visit->client_id    = $request->client_id;
        $visit->name         = $request->name;
        $visit->state        = $request->state;
        $visit->municipality = $request->municipality;
        $visit->street       = $request->street;
        $visit->neighborhood = $request->neighborhood;
        $visit->zipcode      = $request->zipcode;
        $visit->phone        = $request->phone;
        $visit->responsable  = $request->responsable;
        $visit->interview    = $request->interview ? true : false;
        $visit->survey_id    = $request->survey_id;

        $visit->save();

        return redirect()->route('visits.index')->withStatus('Visita registrada correctamente');

    }

    public function import(Request $request){

        $validator = Validator::make($request->all(),[
            'client_id'             => 'required|numeric|exists:clients,id',
            'survey_id'             => 'required|numeric|exists:surveys,id',
            'parse_csv'             => 'required|array',
            'parse_csv.*.Nombre'    => 'required',
            'parse_csv.*.estado'    => 'required',
            'parse_csv.*.municipio' => 'required',
            'parse_csv.*.calle'     => 'required',
            'parse_csv.*.numero'    => 'required|numeric',
            'parse_csv.*.colonia'   => 'required',
            'parse_csv.*.cp'        => 'nullable|numeric|digits:5',
            'parse_csv.*.telefono'  => 'nullable|numeric'
        ]);

        if($validator->fails())
            return response()->json(['errors' => $validator->errors()]);

        foreach ($request->parse_csv as $data) {
            $visit = new Visit();

            $visit->client_id    = $request->client_id;
            $visit->name         = $data['Nombre'];
            $visit->state        = $data['estado'];
            $visit->municipality = $data['municipio'];
            $visit->street       = $data['calle'];
            $visit->neighborhood = $data['colonia'];
            $visit->zipcode      = $data['cp'];
            $visit->phone        = $data['telefono'];
            $visit->responsable  = $data['responsable'];
            $visit->interview    = true;
            $visit->survey_id    = $request->survey_id;

            $visit->save();
        }

        return response()->json(['status' => 200],200);

    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['role:Administrador','auth'])->prefix('users')->name('users.')->group(function(){
    Route::get('/','UserController@index')->name('index');
    Route::get('authenticated','UserController@authenticated')->name('authenticated');
    Route::get('/create','UserController@create')->name('create');
    Route::get('/edit/{id}','UserController@edit')->name('edit')->where('id','[0-9]+');

    Route::post('/store','UserController@store')->name('store');
    Route::post('/update/{id}','UserController@update')->name('update')->where('id','[0-9]');
});

Route::middleware(['role:Administrador','auth'])->prefix('clients')->name('clients.')->group(function(){
    Route::get('/','ClientController@index')->name('index');
    Route::get('/create','ClientController@create')->name('create');
    Route::get('/edit/{id}','ClientController@edit')->name('edit')->where('id','[0-9]+');

    Route::post('/store','ClientController@store')->name('store');
    Route::post('/update/{id}','ClientController@update')->name('update')->where('id','[0-9]+');
});

Route::middleware(['role:Administrador','auth'])->prefix('verifiers')->name('verifiers.')->group(function(){
    Route::get('/','VerifierController@index')->name('index');
    Route::get('/create','VerifierController@create')->name('create');
    Route::get('/edit/{id}','VerifierController@edit')->name('edit')->where('id','[0-9]+');

    Route::post('/store','VerifierController@store')->name('store');
    Route::post('/update/{id}','VerifierController@update')->name('update')->where('id','[0-9]+');
});

Route::middleware(['role:Administrador','auth'])->prefix('surveys')->name('surveys.')->group(function(){
    Route::get('/','SurveyController@index')->name('index');
    Route::get('/create','SurveyController@create')->name('create');
    Route::get('/show/{id}','SurveyController@show')->name('show')->where('id','[0-9]+');

    Route::post('/store','SurveyController@store')->name('store');
});

Route::middleware(['role:Administrador','auth'])->prefix('visits')->name('visits.')->group(function(){
    Route::get('/','VisitController@index')->name('index');
    Route::get('/create','VisitController@create')->name('create');

    Route::post('/store','VisitController@store')->name('store');
    Route::post('/import/store','VisitController@import')->name('import');
});

@extends('layouts.app')
@section('title','Indexmerca - Encuestas')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <create-survey csrf="{{ csrf_token() }}"></create-survey>

@endsection

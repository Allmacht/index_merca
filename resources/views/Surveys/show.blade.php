@extends('layouts.app')
@section('title','Indexmerca - Encuestas')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <show-survey v-bind:survey="{{ json_encode($survey) }}"></show-survey>

@endsection

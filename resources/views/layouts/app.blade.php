<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title','Indexmerca')</title>
    <script src="{{ asset('js/all.min.js')}} "></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body @auth style="background-color:#FCFCFC;" @endauth>
    <div id="app">
        @auth
            <navbar csrf="{{ csrf_token() }}"></navbar>
        @endauth

        <main @auth style="margin-left: 150px;" @endauth>
            @yield('content')
        </main>
    </div>

    @include('Notifications')

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('js/notifications.js') }}"></script>
    @yield('scripts')
</body>

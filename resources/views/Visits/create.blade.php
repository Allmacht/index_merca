@extends('layouts.app')
@section('title','Indexmerca - Visitas')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <create-visit v-bind:clients="{{ json_encode($clients) }}" v-bind:surveys="{{ json_encode($surveys) }}" csrf="{{ csrf_token() }}"></create-visit>

@endsection

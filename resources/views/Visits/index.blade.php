@extends('layouts.app')
@section('title','Indexmerca - Visitas')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <div class="container-fluid content">
        <div class="row">
            <div class="col-12 form-row">
                <div class="col-lg-6">
                    <form action="">
                        <div class="input-group col-lg-8">
                            <input type="text" name="search" class="form-control form-search" placeholder="Buscar" value="{{$search}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary btn-search" type="button" id="button-addon2">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="#" data-toggle="modal" data-target="#import" class="btn btn-new px-4">
                        {{__('IMPORTAR VISITAS')}}
                    </a>
                    <a href="{{route('visits.create')}}" class="btn btn-new px-4">
                        {{__('REGISTAR VISITA')}}
                    </a>
                </div>
            </div>

            @if($visits->count())
                <div class="col-12 table-responsive mt-5">
                    <table class="table table-index">
                        <thead>
                            <tr class="text-center">
                                <th>{{__('Cliente')}}</th>
                                <th>{{__('Domicilio')}}</th>
                                <th>{{__('Estado')}}</th>
                                <th>{{__('Municipio')}}</th>
                                <th>{{__('Estatus')}}</th>
                                <th>{{__('Acciones')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($visits as $visit)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$visit->business_name}}</td>
                                    <td class="align-middle text-truncate">{{$visit->street}}</td>
                                    <td class="align-middle text-truncate">{{$visit->state}}</td>
                                    <td class="align-middle text-truncate">{{$visit->municipality}}</td>
                                    <td class="align-middle text-truncate">
                                        @if($visit->status == 'Pending')
                                            {{__('Pendiente')}}
                                        @elseif ($visit->status == 'Fail')
                                            {{__('No existe')}}
                                        @elseif ($visit->status == 'Success')
                                            {{__('Exitoso')}}
                                        @endif
                                    </td>
                                    <td class="align-middle text-truncate">
                                        <a href="#" class="btn btn-action">
                                            <i class="fas fa-eye fa-sm"></i>
                                        </a>
                                        <a href="#" class="btn btn-action">
                                            <i class="fas fa-pencil-alt fa-sm"></i>
                                        </a>
                                        <button class="btn btn-action">
                                            <i class="fas fa-trash-alt fa-sm"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{ $visits->appends(['search' => $search])->links() }}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5">
                    <i class="fas fa-map-marker-alt fa-4x icon-records"></i>
                    <h6 class="text-muted text-truncate mt-2"><strong>{{__('SIN REGISTROS')}}</strong></h6>
                </div>
            @endif
        </div>
        <import-visits v-bind:clients="{{ json_encode($clients) }}" v-bind:surveys="{{ $surveys }}"></import-visits>
    </div>


@endsection

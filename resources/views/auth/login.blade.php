@extends('layouts.app')
@section('title','Indexmerca - Login')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/auth/login.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-6 col-sm-12 mx-auto div-login shadow rounded pt-5 pb-3">
                <form class="col-12" action="" method="POST">
                    @csrf
                    <div class="col-12 mb-4 text-center">
                        <img src="{{asset('images/indexmerca.png')}}" class="img-fluid" alt="">
                    </div>
                    <div class="form-group px-2">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Usuario" required>
                    </div>
                    <div class="form-group px-2">
                        <input type="password" class="form-control @error('email') is-invalid @enderror" name="password" placeholder="Contraseña" required>
                    </div>
                    <div class="px-2 mt-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                {{__('Recuérdame')}}
                            </label>
                        </div>
                    </div>
                    <div class="px-2 mt-3">
                        <button type="submit" class="btn btn-block rounded-0 btn-submit shadow">{{__('INICIAR SESIÓN')}}</button>
                    </div>
                </form>
                <div class="px-2 div-link text-center">
                    <a href="" class="btn btn-link text-decoration-none link-password">
                        {{__('¿Olvidaste tu contraseña?')}}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

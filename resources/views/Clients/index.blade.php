@extends('layouts.app')
@section('title','Indexmerca - Clientes')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid content">
        <div class="row">
            <div class="col-12 form-row">
                <div class="col-lg-6">
                    <form action="">
                        <div class="input-group col-lg-8">
                            <input type="text" name="search" class="form-control form-search" placeholder="Buscar" value="{{$search}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary btn-search" type="button" id="button-addon2">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="{{route('clients.create')}}" class="btn btn-new px-4">
                        {{__('REGISTAR CLIENTE')}}
                    </a>
                </div>
            </div>

            @if($clients->count())
                <div class="col-12 table-responsive mt-5">
                    <table class="table table-index">
                        <thead>
                            <tr class="text-center">
                                <th class="text-truncate">{{__('Nombre / Razón Social')}}</th>
                                <th class="text-truncate">{{__('Domicilio')}}</th>
                                <th class="text-truncate">{{__('Estado')}}</th>
                                <th class="text-truncate">{{__('Acciones')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clients as $client)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$client->business_name}}</td>
                                    <td class="align-middle text-truncate">{{$client->street}}</td>
                                    <td class="align-middle text-truncate">{{$client->state}}</td>
                                    <td class="align-middle text-truncate">
                                        <a href="#" class="btn btn-action">
                                            <i class="fas fa-eye fa-sm"></i>
                                        </a>
                                        <a href="{{route('clients.edit', ['id' => $client->id])}}" class="btn btn-action">
                                            <i class="fas fa-pencil-alt fa-sm"></i>
                                        </a>
                                        <button class="btn btn-action">
                                            <i class="fas fa-trash-alt fa-sm"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{ $clients->appends(['search' => $search])->links() }}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5">
                    <i class="fas fa-map-marker-alt fa-4x icon-records"></i>
                    <h6 class="text-muted text-truncate mt-2"><strong>{{__('SIN REGISTROS')}}</strong></h6>
                </div>
            @endif
        </div>
    </div>

@endsection

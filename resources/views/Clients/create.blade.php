@extends('layouts.app')
@section('title','Indexmerca - Clientes')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <create-client csrf="{{ csrf_token() }}"></create-client>

@endsection
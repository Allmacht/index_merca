@extends('layouts.app')
@section('title','Indexmerca - Clientes')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <edit-client v-bind:client="{{ json_encode($client) }}" csrf="{{ csrf_token() }}"></edit-client>

@endsection

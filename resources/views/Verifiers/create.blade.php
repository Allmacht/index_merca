@extends('layouts.app')
@section('title','Indexmerca - Verificadores')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <create-verifier csrf="{{ csrf_token() }}"></create-verifier>

@endsection

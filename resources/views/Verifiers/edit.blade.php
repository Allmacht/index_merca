@extends('layouts.app')
@section('title','Indexmerca - Verificadores')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <edit-verifier csrf="{{ csrf_token() }}" v-bind:verifier="{{ json_encode($verifier) }}"></edit-verifier>

@endsection

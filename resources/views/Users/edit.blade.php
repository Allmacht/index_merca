@extends('layouts.app')
@section('title','Indexmerca - Usuarios')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <edit-user
        csrf="{{ csrf_token() }}"
        v-bind:user="{{ json_encode($user) }}"
        v-bind:role="{{ json_encode($role) }}"
        v-bind:clients="{{ json_encode($clients) }}"
        >
    </edit-user>

@endsection

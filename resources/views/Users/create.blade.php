@extends('layouts.app')
@section('title','Indexmerca - Usuarios')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')

    <create-user csrf="{{ csrf_token() }}" v-bind:clients="{{ json_encode($clients) }}"></create-user>

@endsection

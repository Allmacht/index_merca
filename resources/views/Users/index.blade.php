@extends('layouts.app')
@section('title','Indexmerca - Usuarios')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/index.css')}}">
@endsection
@section('content')

    <div class="container-fluid content">
        <div class="row">
            <div class="col-12 form-row">
                <div class="col-lg-6">
                    <form action="">
                        <div class="input-group col-lg-8">
                            <input type="text" name="search" class="form-control form-search" placeholder="Buscar" value="{{$search}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary btn-search" type="button" id="button-addon2">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="{{route('users.create')}}" class="btn btn-new px-4">
                        {{__('REGISTAR USUARIO')}}
                    </a>
                </div>
            </div>

            @if($users->count())
                <div class="col-12 table-responsive mt-5">
                    <table class="table table-index">
                        <thead>
                            <tr class="text-center">
                                <th><strong>{{__('Nombre')}}</strong></th>
                                <th><strong>{{__('Correo')}}</strong></th>
                                <th><strong>{{__('Rol')}}</strong></th>
                                <th><strong>{{__('Estatus')}}</strong></th>
                                <th><strong>{{__('Acciones')}}</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr class="text-center">
                                    <td class="align-middle text-truncate">{{$user->name}}</td>
                                    <td class="align-middle text-truncate">{{$user->email}}</td>
                                    <td class="align-middle text-truncate">
                                        @foreach ($user->getRoleNames() as $rol)
                                            {{$rol}}
                                        @endforeach
                                    </td>
                                    <td class="align-middle text-truncate">
                                        {{$user->status ? 'Activo':'Inactivo'}}
                                    </td>
                                    <td class="align-middle text-truncate">
                                        <a href="{{route('users.edit',['id' => $user->id])}}" class="btn btn-action">
                                            <i class="fas fa-pencil-alt fa-sm"></i>
                                        </a>
                                        <button class="btn btn-action">
                                            <i class="fas fa-trash-alt fa-sm"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="col-12">
                        <div class="col-md-8 col-sm-12 float-left form-inline select-count">

                        </div>
                        <div class="col-md-4 col-sm-12 float-right">
                            {{ $users->appends(['search' => $search])->links() }}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-12 text-center mt-5">
                    <i class="fas fa-map-marker-alt fa-4x icon-records"></i>
                    <h6 class="text-muted text-truncate mt-2"><strong>{{__('SIN REGISTROS')}}</strong></h6>
                </div>
            @endif
        </div>
    </div>

@endsection

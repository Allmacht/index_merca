<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Administrador = Role::create([
            'name' => 'Administrador'
        ]);

        $Cliente = Role::create([
            'name' => 'Cliente'
        ]);
        
        $Supervisor  = Role::create([
            'name' => 'Supervisor'
        ]);
    }
}

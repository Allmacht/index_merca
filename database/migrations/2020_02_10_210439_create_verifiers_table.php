<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('last_name');
            $table->enum('gender',['Male','Female']);
            $table->integer('age');
            $table->enum('civil_status',['Single','Married']);
            $table->boolean('vehicle');
            $table->string('state');
            $table->string('municipality');
            $table->string('phone');
            $table->string('bank');
            $table->string('bank_account');
            $table->enum('status',['Active','Inactive','Pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifiers');
    }
}
